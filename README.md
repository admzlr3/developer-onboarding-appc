# Developer Onboarding

This document is intended as a guidance for new developers joining our teams at [APPC](https://confluence.bbl.int/pages/viewpage.action?pageId=1164465704).
If you know something that's missing feel free to send us a Pull-Request.

![](docs/image.png)

## Step 1 - **Goal:** Get and understanding of Bendigo Bank.

- [Who we are?](https://bendigoadelaidebank.sharepoint.com/sites/Staff/SitePages/Who-We-Are.aspx)
- [Brand Architecture](1517966-BAB-Brand Architecture-condensed v2.pdf)
- [Technology Team Structure](Technology Team_Practices Services Org Structure_effective 28 September 2020.pdf)
- [Product and Pricing Service](https://confluence.bbl.int/display/PPCS/Home+Page)
- [Product and Pricing Design Authority Presentation](DA Presentation - Product Pricing v0_4.pdf)

## Step 2 - **Goal:** Get your access for all required BEN Systems and complete all Mandatory trainings.

- [Zafin UI] "Mail to be sent to Neil for BBL and BBL DTL admin privilege approval Sample SR_1338306
- [Zendesk] Request raised with Smitha Nair
- [IBM control desk or ICD] You should have Default access , go to [intranet](http://intranet/) and check your access.
- [IBM Control Desk queue for APPC] ACCPROC.PPCOMM
- [Request for Virtual Machine] Refert ICD Sample SR_1338281
- [Slack](https://confluence.bbl.int/display/ESS/Request+for+Slack+Access)
  For devops/cloud related questions/clarifications/guidance, raise it here
  - Dev-Ops related, use the pl-de-general channel and you can tag devops-oncall
  - Cloud related (e.g AWS), use the pl-cp-general channel and you can tag cloud-oncall
- [Slack Search Best Practices](https://slack.com/intl/en-au/blog/productivity/shrinking-the-haystack-how-to-narrow-search-results-in-slack)
- [AWS](https://confluence.bbl.int/display/PLT/AWS+User+Access+Request)
- [What are our AWS accounts](https://splunk.bbl.int/en-GB/app/ben_service-cloud-platforms_shd/ben_aws_account_info_with_filters?form.tok_cc_filter=*&form.tok_accid_filter=*&form.tok_accname_filter=*&form.tok_cloudenv_filter=NONPROD&form.tok_rn_filter=*&form.tok_memname_filter=alphin)
- [JIRA](https://confluence.bbl.int/display/ATL/Topic+3%3A+Jira+access)
- [APPC JIRA Boards](https://jira.bbl.int/projects/APPC/summary)
- [Confluence](https://confluence.bbl.int/display/ATL/Confluence+Essentials)
- [Timesheets](https://jira.bbl.int/secure/Tempo.jspa#/my-work)
- [Compliance Trainings](https://benu.edcast.com/)
- [Distribution List access: 
Access to the DL
  TTAccountProcessingTTAccountProcessingProductPricingCommissions@bendigoadelaide.com.auProductPricingCommissions@bendigoadelaide.com.au] Raise ticket in ICD as per sample Ticket SR_1338298
- [BBL Admin Account for production admin]

## Step 3 - **Goal:** get understanding of BEN and internal Systems and procedures.

- [RFS-B](https://bendigoadelaidebank.sharepoint.com/:w:/r/sites/ConsumerProcessing/ProceduresLibrary/_layouts/15/Doc.aspx?sourcedoc=%7BAE51C401-E53A-43FF-BDB7-5339AB40B935%7D&file=Self%20Paced%20-%20RFS-B%20Module.doc&action=default&mobileredirect=true&DefaultItemOpen=1)
- [BDS](http://portal.bbl.int/icentral/Manuals/index.aspx)
- [CSS](http://portal.bbl.int/icentral/Manuals/index.aspx)
- [LAPS](https://bendigoadelaidebank.sharepoint.com/sites/Consumer-Lending-Support/Shared%20Documents/Lending%20Support%20-%20Bendigo/Consumer%20Lending%20Manual%20Linked%20Documents/LAPS_User_Guide.pdf?web=1)
- [COMLaps](https://bendigoadelaidebank.sharepoint.com/sites/Business/Business%20Banking%20Support%20Documents/ComLAPS%20User%20Guide%20-%202021.pdf?web=1)
- [LINX]
- [Best place to understand how things are done in the Branch](http://portal.bbl.int/icentral/Manuals/index.aspx)

## Step 4 - **Goal:** Get understanding Agile cadences, processes and procedures.
- [6 Iterations View](https://confluence.bbl.int/display/PPCS/6+Iteration+view+-+Delivery+team+1+and+2)
- [Sprint Events](https://confluence.bbl.int/display/PPCS/APPC+-+Sprint+Events)

## Step 5 - **Goal:** understand how to develop Microservices & API at BEN

- [Developer Laptop Setup Guide for Windows](https://confluence.bbl.int/pages/viewpage.action?pageId=1164448432)
- [Nodejs Framework Developers Guide](https://confluence.bbl.int/display/PLT/API+-+Node.js+Framework+Developers+Guide)
- [What is BBOX](https://confluence.bbl.int/display/PLT/DO+-+BBOX)
- [What is Tentacles](https://confluence.bbl.int/display/PLT/DO+-+Tentacles+Pattern)
- [API Testing using Postman](https://confluence.bbl.int/display/PLT/API+-+Postman+user-guide+for+testers)
- [Training Course for roles](https://confluence.bbl.int/display/PLT/API+-+Training+Courses+for+Roles)
- [Bendigo Academy](https://gitlab.com/bendigobank/devopsacademy/bendigo-academy/-/blob/master/classes/01-melbourne/README.md)

## Step 6 - **Goal:** understand how to monitor and support applications at BEN

- [Incident Management](TBD)
- [Problem Management](TBD)
- [Change Management](TBD)
- [Release Management](TBD)
- [Monitoring Toolsets at BEN](https://confluence.bbl.int/display/PLT/Copy+of+Software+Toolsets)

## Step 7 - **Goal:** understand Zafin deployment and support process

- [Zafin Documentation](https://bendigoadelaidebank.sharepoint.com/sites/BEN22-Core-Banking/Shared%20Documents/Forms/AllItems.aspx?FolderCTID=0x0120009D6C10FA5B3CB24293C24D516ED31B3C&id=%2Fsites%2FBEN22%2DCore%2DBanking%2FShared%20Documents%2FProduct%20%26%20Pricing%2FZafin%20documents&viewid=62b90800%2De912%2D40c3%2D9b83%2D5f8d202862c2)
- [Zafin Contact Point](https://bendigoadelaidebank.sharepoint.com/sites/BEN22-Core-Banking/Shared%20Documents/Forms/AllItems.aspx?RootFolder=%2Fsites%2FBEN22%2DCore%2DBanking%2FShared%20Documents%2FProduct%20%26%20Pricing%2FOSM%20Documentation%2FOSM%20Analysis&FolderCTID=0x0120009D6C10FA5B3CB24293C24D516ED31B3C)
- [Zafin Trainings - Reach out to Neil Brown or would be posted shortly on BENU](TBD)
- [Zafin Release and Deployment](https://bendigoadelaidebank.sharepoint.com/sites/BEN22-Core-Banking/Shared%20Documents/Forms/AllItems.aspx?FolderCTID=0x0120009D6C10FA5B3CB24293C24D516ED31B3C&id=%2Fsites%2FBEN22%2DCore%2DBanking%2FShared%20Documents%2FProduct%20%26%20Pricing%2FZafin%20documents&viewid=62b90800%2De912%2D40c3%2D9b83%2D5f8d202862c2)
- [Zafin Support Process](TBD)

## Step 8 - **Goal:** understand process mapping using holocentric

- [Processes documented at BENDIGO](https://bendigoadelaidebank.sharepoint.com/:f:/r/sites/TT-Product-Pricing-Initiative/Shared%20Documents/Rewards%20Saver%20and%20Foundation%20Release/Process%20Maps?csf=1&web=1&e=V2ClLx)
- [Holocentric Access] Raise a request with IT Helpdesk via Self Service Portal on Intranet.
- [Holocentric Training](https://ourvolaris-my.sharepoint.com/personal/jay_waugh_holocentric_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fjay%5Fwaugh%5Fholocentric%5Fcom%2FDocuments%2FRecordings%2FHolocentric%20Content%20Author%201%20Training%2D20220223%5F112946%2DMeeting%20Recording%2Emp4&parent=%2Fpersonal%2Fjay%5Fwaugh%5Fholocentric%5Fcom%2FDocuments%2FRecordings)

## Step 9 - **Goal:** <TBD>

## Step 10 - **Goal:** Keep Learning (Links from around the web, the list is endless, dont forget to contribute)

## Basics

- [How The Web Works](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/How_the_Web_works)
- [HTTP on Wikipedia](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)
- [What Happens When](https://github.com/alex/what-happens-when/blob/master/README.rst)
- [What to learn next](https://roadmap.sh/)
- [An Introduction To HTTP Basics](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)
- [Choosing an HTTP Status Code — Stop Making It Hard](https://www.codetinkerer.com/2015/12/04/choosing-an-http-status-code.html)
- [TCP/IP Protocol Fundamentals Explained with a Diagram](http://www.thegeekstuff.com/2011/11/tcp-ip-fundamentals)
- [Google Developer Training For The Web](https://developers.google.com/training/web/)
- [Visualising data structures and algorithms through animation](https://visualgo.net/)
- [HTTP: The Protocol Every Web Developer Must Know](https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-1--net-31177)
- [An Introduction to Networking Terminology, Interfaces, and Protocols](https://www.digitalocean.com/community/tutorials/an-introduction-to-networking-terminology-interfaces-and-protocols)
- [How Browser Caching Works](https://thesocietea.org/2016/05/how-browser-caching-works/)
- [The Absolute Minimum Every Software Developer Absolutely, Positively Must Know About Unicode and Character Sets](http://www.joelonsoftware.com/articles/Unicode.html)
- [What Every Programmer Should Know About Memory](https://www.akkadia.org/drepper/cpumemory.pdf)
- [What Every Developer Should Know About Time](https://unix4lyfe.org/time/?v=1)
- [A story about an angry carrot and a floating point fairy](http://blog.ruslans.com/2014/12/a-story-about-angry-carrot-and-floating.html)

# Design And Architecture Of Software

- [Clean Code](https://www.amazon.com/gp/product/0132350882/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=0132350882&linkCode=as2&tag=sdrl-20)
- [Object Design Style Guide](https://www.manning.com/books/object-design-style-guide)
- [Patterns of Enterprise Application Architecture](https://www.amazon.de/Patterns-Enterprise-Application-Architecture-Martin/dp/0321127420/ref=sr_1_1?ie=UTF8&qid=1476887236&sr=8-1&keywords=patterns+of+enterprise+application+architecture)
- [Domain Driven Design](https://www.amazon.de/Domain-Driven-Design-Tackling-Complexity-Software/dp/0321125215/ref=sr_1_1?ie=UTF8&qid=1476887211&sr=8-1&keywords=domain+driven+design)
- [Introduction to CQRS](http://www.codeproject.com/Articles/555855/Introduction-to-CQRS)
- [CQRS and Event Sourcing](https://www.youtube.com/watch?v=JHGkaShoyNs)
- [Your Code as a Crime Scene](https://pragprog.com/book/atcrime/your-code-as-a-crime-scene)
- [Awesome API](https://github.com/Kikobeats/awesome-api)
- [An introduction to APIs](https://zapier.com/learn/apis/)
- [Architectural Katas](http://nealford.com/katas/)

## Javascript

- [JS for Cats](http://jsforcats.com/)
- [JavaScript for impatient programmers](https://exploringjs.com/impatient-js)
- [You don't know JS](https://github.com/getify/You-Dont-Know-JS)
- [Eloquent Javascript](http://eloquentjavascript.net/)
- [Essential Javascript Patterns](https://addyosmani.com/resources/essentialjsdesignpatterns/book/)
- [MDN - Mozilla Developer Network](https://developer.mozilla.org/de/)
- [CSS-Tricks](https://css-tricks.com/)
- [Netflix UI Engineers Youtube Channel](https://www.youtube.com/channel/UCGGRRqAjPm6sL3-WGBDnKJA)
- [Don't make me think - Web Usability](https://www.amazon.de/Dont-make-think-Usability-intuitive/dp/3826697057/ref=sr_1_1?ie=UTF8&qid=1476888649&sr=8-1&keywords=dont+make+me+think)

## Tools

- [gitlab](https://docs.gitlab.com/ee/topics/use_gitlab.html)
- [Atlassian git Tutorials](https://www.atlassian.com/git/tutorials/)
- [Interactive git Cheatsheet](http://ndpsoftware.com/git-cheatsheet.html)
- [Twelve Benefits of Writing Unit Tests First](http://sd.jtimothyking.com/2006/07/11/twelve-benefits-of-writing-unit-tests-first/)
- [A Beginner's Guide to npm](https://www.sitepoint.com/beginners-guide-node-package-manager/)

## Environment

- [Basic UNIX commands](http://mally.stanford.edu/~sr/computing/basic-unix.html)
- [Explain Shell - Let it explain you what's going on](http://explainshell.com/)
- [Effective DevOps](http://shop.oreilly.com/product/0636920039846.do)
- [Docker Docs](https://docs.docker.com/)
- [Docker Swarms Introduction](https://docs.docker.com/get-started/part4/)
- [What is Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)
- [Request routing with Traefik](https://doc.traefik.io/traefik/getting-started/concepts/)
- [How to write Ansible Playbooks](https://www.digitalocean.com/community/tutorials/configuration-management-101-writing-ansible-playbooks)
- [Ansible Docs](https://docs.ansible.com/)
- [Artifactory User Guide](https://www.jfrog.com/confluence/pages/viewpage.action?pageId=46107472)

## Persistence

- [SQL Performance Explained - Short and useful book about SQL performance for developers](https://www.amazon.de/SQL-Performance-Explained-Entwickler-SQL-Performance/dp/3950307818)
- [Use the index, Luke - a guide to database performance for developers](http://use-the-index-luke.com/)
- [The Little Redis Book](https://github.com/karlseguin/the-little-redis-book)

## Apache Kafka

- [Kafka as a Platform: the Ecosystem from the Ground Up](https://www.youtube.com/watch?v=WpfJ86_DYfY)
- [Apache Kafka in a Nutshell](https://medium.com/swlh/apache-kafka-in-a-nutshell-5782b01d9ffb)
- [Why Avro for Kafka Data?](https://www.confluent.de/blog/avro-kafka-data/)
- [Deal with Failure in an Event-Driven System](https://www.confluent.io/blog/using-kafka-merge-purge-to-simplify-kafka-failures/)

## Processes & Culture

- [The Agile Manifesto](http://agilemanifesto.org/)
- [Agile Retrospectives](https://pragprog.com/book/dlret/agile-retrospectives)
- [The Phoenix Project](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262592)
- [Team Geek - A Software Developer's Guide to Working Well with Others](https://www.amazon.com/Team-Geek-Software-Developers-Working/dp/1449302440/)

## Information Security

- [OWASP - Open Web Application Security Project](https://www.owasp.org/index.php/Main_Page)
- [A Gentle Introduction to Application Security](https://paragonie.com/blog/2015/08/gentle-introduction-application-security)
- [Juice Shop - A vulnerable web app to play around](https://github.com/bkimminich/juice-shop)

## Best Practices

- [The Best Code is No Code At All](https://blog.codinghorror.com/the-best-code-is-no-code-at-all/)
- [The Pragmatic Programmer](https://pragprog.com/titles/tpp20/the-pragmatic-programmer-20th-anniversary-edition/)
- [The Clean Coder](https://www.oreilly.com/library/view/the-clean-coder/9780132542913/)
- [Simple Made Easy](https://www.youtube.com/watch?v=rI8tNMsozo0)
- [A Philosophy of Software Design](https://www.youtube.com/watch?v=bmSAYlu0NcY)
- [Refactoring: Improving the Design of Existing Code](https://www.amazon.com/exec/obidos/ASIN/0201485672/codihorr-20)
- [The full stackoverflow developer](https://www.christianheilmann.com/2015/07/17/the-full-stackoverflow-developer/)
- [Measure anything, measure everything](https://codeascraft.com/2011/02/15/measure-anything-measure-everything/)
- Go to a local user group
- Read open-source code on [GitHub](https://www.github.com)
- Never stop building stuff
